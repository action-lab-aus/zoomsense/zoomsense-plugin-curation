# Bicara Plugin - Curation

Bicara Curation Plugin is responsible for generating a list of curated assets when a session ends. It generates a list of suggested video & audio clips when a session is transcribed and allows triggering curation assets generation via calling the AWS Elemental MediaConvert endpoint.

- `onCurationAssetAdded`: Triggered when a new curation asset generation task is added to the `currationAsset` node with `s3UrlVideo`, `startTimecode` & `endTimecode`, which clips the session recording .mp4 file based on the start and end timestamp provided
- `onRecordingTranscribed`: When the transcription is generated and status changed to `transcribed`, start creating suggested curation assets for the current session
- `curationOnCompletedSns`: AWS SNS subscription endpoint called when a new curation asset is generated and saved to the S3 Bucket with the `curated.mp4` suffix. Update the status under the `data/curationAsset` node along with the s3ObjKey

More information about the AWS MediaConvert, Amazon SNS, and S3 Event Notification integration with Bicara can be found under [Bicara Core Step 2.3 & 2.4](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/3_QuickStart_Vonage.md).

## Local Development

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder (configuration details can be found under [Bicara Core Step 6](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/7_QuickStart_Functions.md)), Firebase Functions can be tested locally using the command:

```
npm run emulators
```

This ensures that the emulator data will be imported when Firebase Emulator starts and exported when the session ends.
