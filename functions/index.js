const { defineString } = require('firebase-functions/params');
awsAccessKey = defineString('AWS_ACCESS_KEY_ID');
awsRegion = defineString('AWS_REGION');
curationCompleteEmailTemplate = defineString('SENDGRID_CURATION_COMPLETED');
mediaconvertEndpoint = defineString('AWS_MEDIACONVERT_ENDPOINT');
mediaconvertQueue = defineString('AWS_MEDIACONVERT_QUEUE');
mediaconvertRole = defineString('AWS_MEDIACONVERT_ROLE');
bucketName = defineString('AWS_BUCKET_NAME');

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.database();
const AWS = require('aws-sdk');

function initAWS() {
  AWS.config.update({
    accessKeyId: awsAccessKey.value(),
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: awsRegion.value(),
  });

  AWS.config.mediaconvert = { endpoint: mediaconvertEndpoint.value() };
}

const _ = require('lodash');
const request = require('request');

/**
 * When the transcription is generated and status changed to `transcribed`, start creating
 * suggested curation assets for the current session
 *
 * @param {String} meetingId Session ID
 */
exports.onRecordingTranscribed2 = functions
  .runWith({
    secrets: ['AWS_SECRET_ACCESS_KEY'],
  })
  .database.ref('/data/transcription/{meetingId}/status')
  .onCreate(async (snapshot, context) => {
    initAWS();
    console.log('DOING');
    try {
      const meetingId = context.params.meetingId;
      const status = snapshot.val();
      // console.log(status);
      if (status === 'transcribed') {
        const meta = (await db.ref(`data/meetingMeta/${meetingId}`).once('value')).val();
        const sections = await getSections(meetingId, meta.actualEndTime);

        // console.log(sections);

        // Create suggested assets from session tags
        const tags = await getSessionTags(meetingId, sections, meta);

        // console.log(tags);

        // Created suggested assets from a few key slides
        const slides = await getSlides(meetingId, sections, meta);

        // console.log(slides);

        // Create suggested curation assets and push to the `curation` data node
        const merged = [...tags, ...slides];

        console.log(`merged`, merged);
        // console.log(sections);

        //if there are no artifacts identified, add one for each section...
        if (merged.length == 0) {
          Object.keys(sections).forEach(sec => {
            // console.log(sec);
            const obj = sections[sec];
            merged.push({
              type: 'auto',
              section: sec,
              startTime: obj.startTime,
              endTime: obj.endTime,
              duration: obj.duration,
            });
          });
        }

        const promises = [];
        merged.forEach(item => {
          const { startTime, endTime } = item;
          let curationObj = { ...item, status: 'suggested', toCoach: false, toParents: false };
          promises.push(db.ref(`data/curation/${meetingId}`).push(curationObj));
        });
        await Promise.all(promises);

        await db.ref(`data/curation/${meetingId}`).update({ suggested: true });
      }
    } catch (error) {
      functions.logger.error('onRecordingTranscribed Error: ', error);
    }
  });

/**
 * Fetch all the tags for the current session for creating suggested curation assets
 *
 * @param {String} meetingId Session ID
 * @param {Object} sections Section objects with startTime, endTime and duration
 * @param {Object} meta Session metadata
 *
 * @returns {Object} Tagged section objects with startTime, endTime and duration
 */
async function getSessionTags(meetingId, sections) {
  const tagging = (await db.ref(`data/tagging/${meetingId}`).once('value')).val();
  let tagged = [];
  let start = null;

  const schedule = (await db.ref(`config/${meetingId}/schedule/schedule`).once('value')).val();

  // const noOfSections = Object.keys(schedule.script).length;

  const metaSections = _.map(schedule.script, 'name');
  // console.log(metaSections);

  // functions.logger.info('sections: ', sections);

  console.log(tagging);

  if (tagging) {
    const lastKey = _.last(Object.keys(tagging));
    Object.keys(tagging).forEach(key => {
      const { data, timestamp } = tagging[key];
      // First occurrence of non zero value, set start
      if (data > 0 && !start) start = timestamp;
      // First occurrence of zero or its the last item in the array when start is not null, set end & push to tagged array
      if ((data < 5 || key === lastKey) && start) {
        const sectionIndex = getSectionIndex(sections, start, timestamp);
        if (sectionIndex >= 0)
          tagged.push({
            type: 'tag',
            section: sectionIndex,
            startTime: start,
            endTime: timestamp,
            duration: timestamp - start,
          });
        start = null;
      }
    });
  }

  return tagged;
}

/**
 * Construct the session map with startTime and endTime for each session
 *
 * @param {String} meetingId Session ID
 * @param {Number} actualEndTime Actual End Time of the Meeting
 *
 * @returns {Object} Section objects with startTime, endTime and duration
 */
async function getSections(meetingId, actualEndTime) {
  const sectionChange = (await db.ref(`data/sectionChange/${meetingId}`).once('value')).val();

  let allHistory = sectionChange['_history'];
  // Append the meeting end time to get the duration for the last section
  allHistory[actualEndTime] = { _currentSection: -1 };
  const keys = Object.keys(allHistory);

  const sectionMap = {};
  for (let i = 1; i < keys.length; i++) {
    const duration = keys[i] - keys[i - 1];
    const prev = allHistory[keys[i - 1]]['_currentSection'];

    if (!sectionMap[prev] || sectionMap[prev].duration < duration)
      sectionMap[prev] = {
        duration,
        startTime: keys[i - 1],
        endTime: keys[i],
      };
  }

  // console.log(sectionMap);

  return sectionMap;
}

/**
 * Fetch all the key slides for the current session for creating suggested curation assets
 *
 * @param {String} meetingId Session ID
 * @param {Object} sections Section objects with startTime, endTime and duration
 * @param {Object} meta Session metadata
 *
 * @returns {Object} Slide section objects with startTime, endTime and duration
 */
async function getSlides(meetingId, sections, meta) {
  const slides = (await db.ref(`data/slides/${meetingId}`).once('value')).val();
  const schedule = (await db.ref(`config/${meetingId}/schedule/schedule`).once('value')).val();

  // const noOfSections = meta.sections.length;
  const noOfSections = Object.keys(schedule.script).length;
  // console.log(Object.keys(schedule));
  // console.log(`sections:${noOfSections}`);

  // console.log(sections);
  const resultArr = [];

  // Loop through the session sections and find the slide timestamps
  for (let i = 0; i < noOfSections; i++) {
    // console.log(i);
    const section = sections[i];
    //if there is timing info for this section
    if (section) {
      let nextSectionIndex = i + 1;

      if (i < noOfSections) {
        while (typeof sections[nextSectionIndex] === 'undefined' && nextSectionIndex < noOfSections - 1)
          nextSectionIndex++;

        // console.log(`ended while loop at ${nextSectionIndex} for ${i}`);
      }

      const nextStart = i < noOfSections - 1 ? sections[nextSectionIndex].startTime : meta.actualEndTime;

      // console.log(`nextStart: ${nextStart}`);

      //get slide timestamps
      const slideMap = getSlideTimestamps(slides, section, nextStart);

      //get all slides in this section
      const sectionSlides = getSectionSlides(schedule, i);
      const noOfSlides = sectionSlides.length;

      // console.log(`no of slides: ${noOfSlides}`);

      // console.log(`for each slide in ${i}`);
      //for each slide, see if its got an input
      for (let j = 0; j < noOfSlides; j++) {
        const current = sectionSlides[j];
        // console.log(slideMap[j]);
        if (slideMap[j] && (current.match(/Goal Review/) || current.match(/Goal Planning/))) {
          resultArr.push({
            ...slideMap[j],
            type: 'slide',
            section: i,
            slide: current.match(/Goal Review/) ? 'Goal Review' : 'Goal Planning',
          });
        }
      }
    } else {
      console.log(`no section timing info for ${i}`);
    }
  }

  // console.log('Finished slides');
  console.log(resultArr);

  return resultArr;
}

/**
 * Get slides for the current section with startTime, endTime and duration
 *
 * @param {Object} slides Slide change and input history data
 * @param {Object} current Current section object
 * @param {Number} nextStart Start timestamp for the next section
 *
 * @returns {Object} Slide objects with startTime, endTime and duration
 */
function getSlideTimestamps(slides, current, nextStart) {
  let allHistory = slides['_history'];
  allHistory = Object.fromEntries(
    Object.entries(allHistory).filter(
      history => history[1]['_slide'] !== undefined && history[0] >= current.startTime && history[0] <= current.endTime,
    ),
  );
  allHistory[nextStart] = { _slide: -1 };

  const keys = Object.keys(allHistory);
  const slideMap = {};

  for (let i = 1; i < keys.length; i++) {
    const duration = keys[i] - keys[i - 1];
    const prev = allHistory[keys[i - 1]]['_slide'];

    if (!slideMap[prev] || slideMap[prev].duration < duration)
      slideMap[prev] = {
        duration,
        startTime: keys[i - 1],
        endTime: keys[i],
      };
  }

  return slideMap;
}

/**
 * For a given start and end timestamp, return the corresponding section index
 *
 * @param {Array} sections Section objects with startTime, endTime and duration
 * @param {Number} start Start timestamp
 * @param {Number} end End timestamp
 *
 * @returns {Number} Section index
 */
function getSectionIndex(sections, start, end) {
  const keys = Object.keys(sections);
  let index = -1;
  for (let i = 0; i < keys.length; i++) {
    const { startTime, endTime } = sections[keys[i]];
    if (start >= startTime && end <= endTime) {
      index = i;
      break;
    }
  }
  return index;
}

/**
 * Fetch all slide content for a specific meeting section
 *
 * @param {*} schedule Session schedule object
 * @param {*} index Session index
 *
 * @returns {Array} An array of slide content for the currents estion
 */
function getSectionSlides(schedule, index) {
  const plugins = schedule.script[index].plugins;
  const tmp = _.find(plugins, plugin => plugin.plugin === 'Slides');

  if (tmp) return tmp.settings.content;
  return [];
}

/**
 * When new curation assets are pushed to the `curationAsset` node, trigger the AWS Media Convert
 * Job to create video clips for parent's learning journey and coach's self-reflection
 *
 * @param {String} meetingId Session ID
 * @param {String} assetId Curated Asset ID
 */
exports.onCurationAssetAdded = functions
  .runWith({
    secrets: ['AWS_SECRET_ACCESS_KEY'],
  })
  .database.ref('/data/curationAsset/{meetingId}/{assetId}')
  .onCreate(async (snapshot, context) => {
    initAWS();
    const { meetingId, assetId } = context.params;
    // Fetch the current plugins object
    const plugins = (await db.ref(`/config/${meetingId}/current/currentState/plugins`).once('value')).val();
    if (plugins['curation'] && plugins['curation'].enabled) {
      // If the curation plugin is enabled for the current session, start the transcription
      const { s3UrlVideo, startTimecode, endTimecode } = snapshot.val();
      if (s3UrlVideo && startTimecode && endTimecode) {
        const response = await createMediaConvertJob(s3UrlVideo, startTimecode, endTimecode, assetId);
        // console.log(response);
        await db.ref(`/mediaJobRef/${response.Job.Id}`).set({ meetingId, assetId });
      }
    }
  });

/**
 * Create a media convert job on AWS with defined parameters
 *
 * @param {String} s3UrlVideo S3 URL of the video
 * @param {String} startTimecode Start timecode for clipping in HH:MM:SS:MS format
 * @param {String} endTimecode End timecode for clipping in HH:MM:SS:MS format
 * @param {String} assetId Curation asset ID
 */
async function createMediaConvertJob(s3UrlVideo, startTimecode, endTimecode, assetId) {
  functions.logger.info('s3UrlVideo: ', s3UrlVideo);
  functions.logger.info('startTimecode: ', startTimecode);
  functions.logger.info('endTimecode: ', endTimecode);

  const filePathArr = s3UrlVideo.split('/');
  const destindation = `s3://${bucketName.value()}/${filePathArr[0]}/${filePathArr[1]}/`;
  const params = getMediaConvertParams(
    `s3://${bucketName.value()}/${s3UrlVideo}`,
    `_${assetId}_curated`,
    destindation,
    startTimecode,
    endTimecode,
  );

  // Create a promise on a MediaConvert object
  return new AWS.MediaConvert({ apiVersion: '2017-08-29' }).createJob(params).promise();
}

/**
 * AWS SNS notification endpoint when a new curation assets is generated and saved to the
 * S3 Bucket. Update the status under the `data/curationAsset` node along with the s3ObjKey
 *
 */
exports.curationOnCompletedSns = functions.https.onRequest(async (req, res) => {
  try {
    // Parse the request payload and extract the notification topic
    const payload = JSON.parse(req.body);
    if (req.header('x-amz-sns-message-type') === 'SubscriptionConfirmation') {
      console.log(payload);
      const url = payload.SubscribeURL;
      await request(url, handleSubscriptionResponse);
      return res.send('OK');
    } else {
      const topic = payload.TopicArn.split(':')[5];
      functions.logger.info('topic: ', topic);

      if (topic === 'TopsCurationCompleteAlert') {
        const s3ObjKey = JSON.parse(payload.Message).Records[0].s3.object.key;
        functions.logger.info('s3ObjKey: ', s3ObjKey);
        const archiveId = s3ObjKey.split('/')[1];
        const assetId = s3ObjKey.split('/')[2].replace('archive_', '').replace('_curated.mp4', '');
        // functions.logger.info('archiveId: ', archiveId);
        // functions.logger.info('assetId: ', assetId);

        // Fetch the coach uid & meeting id from the archive reference node
        const snapshot = await db.ref(`archiveRef/${archiveId}`).once('value');
        const { meetingId } = snapshot.val();

        // Update the curationAsset data node with the transcribed status
        await db.ref(`data/curationAsset/${meetingId}/${assetId}`).update({
          status: 'generated',
          s3UrlCurated: s3ObjKey,
        });

        // Update the remaining curation tasks
        await calculateRemaining(meetingId);
      } else if (topic === 'MediaConvertJobErrorAlert') {
        functions.logger.info('Processing Error');
        const jobId = JSON.parse(payload.Message).detail.jobId;

        // const s3ObjKey = JSON.parse(payload.Message).Records[0].s3.object.key;
        // functions.logger.info('s3ObjKey: ', s3ObjKey);
        // const archiveId = s3ObjKey.split('/')[1];
        // const assetId = s3ObjKey.split('/')[2].replace('archive_', '').replace('_curated.mp4', '');
        // functions.logger.info('archiveId: ', archiveId);
        // functions.logger.info('assetId: ', assetId);

        // Fetch the coach uid & meeting id from the archive reference node
        // const snapshot = await db.ref(`archiveRef/${archiveId}`).once('value');
        // const { meetingId } = snapshot.val();
        // console.log(jobId);

        const asset = (await db.ref(`/mediaJobRef/${jobId}`).once('value')).val();
        // Update the curationAsset data node with the transcribed status
        await db.ref(`data/curationAsset/${asset.meetingId}/${asset.assetId}`).update({
          status: 'error',
        });

        await calculateRemaining(asset.meetingId);
      }

      res.status(200).send('Curated asset generated and saved to S3');
    }
  } catch (error) {
    functions.logger.error(JSON.stringify(error));
    res.status(500).send(error);
  }
});

async function calculateRemaining(meetingId) {
  const remaining = (await db.ref(`data/curationAsset/${meetingId}/remaining`).once('value')).val();
  await db.ref(`data/curationAsset/${meetingId}/remaining`).set(remaining - 1);
  if (remaining - 1 === 0) {
    const { patientUid } = (await db.ref(`data/meetingMeta/${meetingId}`).once('value')).val();
    const { email: parentEmail, name: parentName } = (await db.ref(`patients/${patientUid}`).once('value')).val();

    //trigger email to client:
    await db.ref(`emails`).push({
      id: patientUid,
      requiresAuth: true,
      email: parentEmail,
      template: curationCompleteEmailTemplate.value(),
      data: {
        parentName,
      },
    });
  }
}

/**
 * Send SendGrid notification emails with optional attachments using Dynamic Templates
 *
 * @param email Target email address
 * @param dynamicId SendGrid dynamic template id
 * @param params SendGrid Handle Bar parameters (e.g. sessionId, coachEmail, curationUrl)
 * @param attachments An array of email attachmenmts
 *
 * @returns SendGrid emailing client response
 */
// async function sendSendGridEmail(email, dynamicId, params, attachments) {
//   functions.logger.info('email: ', email);
//   functions.logger.info('dynamicId: ', dynamicId);
//   functions.logger.info('params: ', params);

//   let msg = {
//     to: email,
//     from: {
//       email: SENDGRID_SENDER,
//       name: SENDGRID_NAME,
//     },
//     templateId: dynamicId,
//     dynamic_template_data: params,
//   };

//   if (attachments) msg.attachments = attachments;

//   const response = await sgMail.send(msg);
//   functions.logger.info(response[0].statusCode);
//   return response;
// }

// AWS SNS endpoint for subscription verficiation
// exports.curationOnCompletedSns = functions.https.onRequest(async (req, res) => {
//   let payload = JSON.parse(req.body);
//   console.log('payload: ', payload);
//   try {
//     if (req.header('x-amz-sns-message-type') === 'SubscriptionConfirmation') {
//       const url = payload.SubscribeURL;
//       await request(url, handleSubscriptionResponse);
//     } else if (req.header('x-amz-sns-message-type') === 'Notification') {
//       console.log(payload);
//     } else {
//       throw new Error(`Invalid message type ${payload.Type}`);
//     }
//   } catch (err) {
//     console.error(err);
//     res.status(500).send('Oops');
//   }
//   res.send('Ok');
// });

const handleSubscriptionResponse = function (error, response) {
  if (!error && response.statusCode === 200) {
    console.log('Yes! We have accepted the confirmation from AWS.');
  } else {
    throw new Error(`Unable to subscribe to given URL`);
  }
};

/**
 * Return the media conver parameters based on the input file url and name modifier
 *
 * @param {String} fileInput S3 video url for generating curation assets
 * @param {String} fileNameModifier Name modifier for the media convert job
 * @param {String} filePath Destination curated asset file path in S3
 * @param {String} startTimecode Start timecode for clipping in HH:MM:SS:MS format
 * @param {String} endTimecode End timecode for clipping in HH:MM:SS:MS format
 */
function getMediaConvertParams(fileInput, fileNameModifier, filePath, startTimecode, endTimecode) {
  return {
    Settings: {
      AdAvailOffset: 0,
      Inputs: [
        {
          FilterEnable: 'AUTO',
          PsiControl: 'USE_PSI',
          FilterStrength: 0,
          DeblockFilter: 'DISABLED',
          DenoiseFilter: 'DISABLED',
          TimecodeSource: 'ZEROBASED',
          VideoSelector: {
            ColorSpace: 'FOLLOW',
            Rotate: 'DEGREE_0',
            AlphaBehavior: 'DISCARD',
          },
          InputClippings: [
            {
              StartTimecode: startTimecode,
              EndTimecode: endTimecode,
            },
          ],
          AudioSelectors: {
            'Audio Selector 1': {
              DefaultSelection: 'DEFAULT',
            },
          },
          FileInput: fileInput,
        },
      ],
      OutputGroups: [
        {
          Name: 'File Group',
          OutputGroupSettings: {
            Type: 'FILE_GROUP_SETTINGS',
            FileGroupSettings: {
              Destination: filePath,
            },
          },
          Outputs: [
            {
              VideoDescription: {
                ScalingBehavior: 'DEFAULT',
                TimecodeInsertion: 'DISABLED',
                AntiAlias: 'ENABLED',
                Sharpness: 50,
                CodecSettings: {
                  Codec: 'H_264',
                  H264Settings: {
                    InterlaceMode: 'PROGRESSIVE',
                    NumberReferenceFrames: 3,
                    Syntax: 'DEFAULT',
                    Softness: 0,
                    GopClosedCadence: 1,
                    GopSize: 90,
                    Slices: 1,
                    GopBReference: 'DISABLED',
                    SlowPal: 'DISABLED',
                    SpatialAdaptiveQuantization: 'ENABLED',
                    TemporalAdaptiveQuantization: 'ENABLED',
                    FlickerAdaptiveQuantization: 'DISABLED',
                    EntropyEncoding: 'CABAC',
                    FramerateControl: 'INITIALIZE_FROM_SOURCE',
                    RateControlMode: 'CBR',
                    CodecProfile: 'MAIN',
                    Telecine: 'NONE',
                    MinIInterval: 0,
                    AdaptiveQuantization: 'HIGH',
                    CodecLevel: 'AUTO',
                    FieldEncoding: 'PAFF',
                    SceneChangeDetect: 'ENABLED',
                    QualityTuningLevel: 'SINGLE_PASS',
                    FramerateConversionAlgorithm: 'DUPLICATE_DROP',
                    UnregisteredSeiTimecode: 'DISABLED',
                    GopSizeUnits: 'FRAMES',
                    ParControl: 'INITIALIZE_FROM_SOURCE',
                    NumberBFramesBetweenReferenceFrames: 2,
                    RepeatPps: 'DISABLED',
                    DynamicSubGop: 'STATIC',
                    Bitrate: 1000000, // 120 Seconds * 1000000 Bps = 14.6 Mb
                  },
                },
                AfdSignaling: 'NONE',
                DropFrameTimecode: 'ENABLED',
                RespondToAfd: 'NONE',
                ColorMetadata: 'INSERT',
              },
              AudioDescriptions: [
                {
                  AudioTypeControl: 'FOLLOW_INPUT',
                  CodecSettings: {
                    Codec: 'AAC',
                    AacSettings: {
                      AudioDescriptionBroadcasterMix: 'NORMAL',
                      Bitrate: 96000,
                      RateControlMode: 'CBR',
                      CodecProfile: 'LC',
                      CodingMode: 'CODING_MODE_2_0',
                      RawFormat: 'NONE',
                      SampleRate: 48000,
                      Specification: 'MPEG4',
                    },
                  },
                  LanguageCodeControl: 'FOLLOW_INPUT',
                },
              ],
              ContainerSettings: {
                Container: 'MP4',
                Mp4Settings: {
                  CslgAtom: 'INCLUDE',
                  CttsVersion: 0,
                  FreeSpaceBox: 'EXCLUDE',
                  MoovPlacement: 'PROGRESSIVE_DOWNLOAD',
                },
              },
              Extension: 'mp4',
              NameModifier: fileNameModifier,
            },
            {
              VideoDescription: {
                ScalingBehavior: 'DEFAULT',
                TimecodeInsertion: 'DISABLED',
                AntiAlias: 'ENABLED',
                Sharpness: 50,
                CodecSettings: {
                  Codec: 'FRAME_CAPTURE',
                  FrameCaptureSettings: {
                    FramerateNumerator: 1,
                    FramerateDenominator: 5,
                    MaxCaptures: 5,
                    Quality: 85,
                  },
                },
                DropFrameTimecode: 'ENABLED',
                ColorMetadata: 'INSERT',
              },
              ContainerSettings: {
                Container: 'RAW',
              },
              Extension: 'jpg',
              NameModifier: fileNameModifier,
            },
            {
              ContainerSettings: {
                Container: 'RAW',
              },
              AudioDescriptions: [
                {
                  AudioSourceName: 'Audio Selector 1',
                  CodecSettings: {
                    Codec: 'MP3',
                    Mp3Settings: {
                      Bitrate: 192000,
                      Channels: 1,
                      RateControlMode: 'CBR',
                      SampleRate: 48000,
                    },
                  },
                },
              ],
              NameModifier: fileNameModifier,
            },
          ],
        },
      ],
    },
    Queue: mediaconvertQueue.value(),
    Role: mediaconvertRole.value(),
    StatusUpdateInterval: 'SECONDS_10',
  };
}
